// whole of  Northland. This file is on my space, but I made it shareable.
// If you want to upload a better shapefile, follow these instructions:
// https://developers.google.com/earth-engine/importing
// var roi = ee.FeatureCollection('users/rogierwesterhoff/Northland_NZTM');

// Simeon was here.
// *sigh*.... he did it again.

var zoom = 9;

// I am using atmospherically corrected Sentinel-2 (_SR),
// that is only available as from half Dec 2018, hence the start date
var startDate = '2018-12-14'; // start date of satellite data acquisition
var endDate = '2019-04-01'; // end date of satellite data acquisition

Map.centerObject(roi,zoom); 

//++++ FUNCTIONS ++++//

//Function to import Sentinel-2, and scaling all bands correctly
function s2ScaleAndBareEssentials(img) {
  var s2 = img.select(['B2','B3','B4','B8'])
                       .divide(10000)
                       .addBands(img.select(['QA60','SCL','MSK_CLDPRB']))
                       .addBands(img.select(['TCI_R','TCI_G','TCI_B']))
                       .addBands(img.metadata('system:time_start'));
  return s2;
}

// Function to mask clouds using the addtional Sentinel-2 QA60 (option 1) or
// SCL and MSK_CLDPRB bands (option2).
var maskClouds = function(image){
 /*
 // option 1:
 var qa = image.select('QA60');
  // Bits 10 and 11 are clouds and cirrus, respectively.
  var cloudBitMask = Math.pow(2, 10);
  var cirrusBitMask = Math.pow(2, 11);
  
  // clear if both flags set to zero.
  //var clear = qa.bitwiseAnd(cloudBitMask).eq(0);
  var clear = qa.bitwiseAnd(cloudBitMask).eq(0).and(
           qa.bitwiseAnd(cirrusBitMask).eq(0));
  
  var mask = clear.eq(1); 
 */
 
 // I am using option 2:
var cloudProb = image.select('MSK_CLDPRB');
var scl = image.select('SCL'); 

var shadow = scl.eq(3); // 3 = cloud shadow
var cirrus = scl.eq(10); // 10 = cirrus
var mask = cloudProb.lt(2).and((cirrus).neq(1)).and((shadow).neq(1)); // thanks Eric Waller for the correction

return image.updateMask(mask);

};

// This function adds vegetation and water index bands to images.
var addIndexBands = function(image) {
  return image
    .addBands(image.normalizedDifference(['B8', 'B4']).rename('NDVI'))
        .addBands(image.expression(
    '2.5 * ((NIR - RED) / (NIR + 6 * RED - 7.5 * BLUE + 1))', {
      'NIR': image.select('B8'),
      'RED': image.select('B4'),
      'BLUE': image.select('B2')
    }).rename('EVI'));
};
//++++ END FUNCTIONS ++++//


// Load Sentinel-2 surface reflectance data (Level 2A).
var s2 = ee.ImageCollection('COPERNICUS/S2_SR')
  .filterDate(startDate,endDate)
  .filterBounds(roi);

s2 = s2.map(s2ScaleAndBareEssentials);
print('s2 ', s2);

var cloudFreeMosaic = s2
  .map(maskClouds)
  .median();

var ndvi = s2
  .map(maskClouds)
  .map(addIndexBands)
  .select('NDVI')
  .median();
  
Map.addLayer(ndvi.clip(roi),
  {min: -1, max: 1, palette: ['blue', 'white', 'green']},
  'Sentinel-2 NDVI',false);
        
var evi = s2
  .map(maskClouds)
  .map(addIndexBands)
  .select('EVI')
  .median();
  
Map.addLayer(evi.clip(roi),
        {min: -1, max: 1, palette: ['blue', 'white', 'green']},
        'Sentinel-2 EVI',false);

// Generation of the rgb composite.
var composite = cloudFreeMosaic 
                .select(['B4','B3','B2']);

//Map.addLayer(cloudFreeMosaic.clip(roi), {bands: ['B4', 'B3', 'B2'], min: 0, max: 0.2},
//            'Sentinel-2 RGB',false);
            
var composite2 = cloudFreeMosaic 
                .select(['TCI_R', 'TCI_G', 'TCI_B']);
            
Map.addLayer(cloudFreeMosaic.clip(roi), {bands: ['TCI_R', 'TCI_G', 'TCI_B'], min: 0, max: 255},
            'Sentinel-2 TCI RGB',true);

/*
// Only uncomment this if you are exporting (this export can long)
// Export the image, specifying scale, region, and larger than default maxPixels.
Export.image.toDrive({
  image: composite.toFloat(),
  description: 'cloudFreeMosaic',
  crs: 'EPSG:4326',
  fileFormat: 'GeoTIFF',
  scale: 10,
  region: roi,
  maxPixels: 1e9
});

Export.image.toDrive({
  image: composite2.toInt(),
  description: 'cloudFreeMosaicTCI',
  crs: 'EPSG:4326',
  fileFormat: 'GeoTIFF',
  scale: 10,
  region: roi,
  maxPixels: 1e9
});

Export.image.toDrive({
  image: ndvi.toFloat(),
  description: 'NDVI',
  crs: 'EPSG:4326',
  fileFormat: 'GeoTIFF',
  scale: 10,
  region: roi,
  maxPixels: 1e9
});

Export.image.toDrive({
  image: cloudFreeMosaic.select('B4').toFloat(),
  description: 'Red',
  crs: 'EPSG:4326',
  fileFormat: 'GeoTIFF',
  scale: 10,
  region: roi,
  maxPixels: 1e9
});
*/
